

#' makeH264VideoFromSelectedFrames
#' 
#' @param inputBaseDirectory path containing "TSeries" subfolders.
#' @param VOI vector of volume indices that should be included in the video.
#' @param plane which plane to include in the video
#' @param nPlanes total number of planes present in image series.
#' @param channel string containing channel to use in video, for example "Ch1" or "Ch2". Must be present in filename.
#' @param videoName string containing name of output video.
makeH264VideoFromSelectedFrames.Direct = function(inputBaseDirectory, VOI, plane, nPlanes, channel, videoName='video.mp4'){
  "%+%" <- function(x,y) paste(x,y,sep='')

  setwd(inputBaseDirectory)
  folders = list.dirs(inputBaseDirectory, recursive=FALSE, full.names=FALSE)
  folders = folders[grep('TSeries', folders)]
  
  files = unname(lapply(folders, function(f) paste(f, list.files(f, pattern = channel %+% ".*tif+$", recursive=FALSE), sep='/')))
  files = lapply(files, function(x) x[seq(plane, length(x), by=nPlanes)]) # select frames for relevant plane
  filesUnrolled = unlist(files)
  
  if (!all(VOI %in% 1:length(filesUnrolled)))
    stop("Cannot find files for some volumes of interest (VOI)! Did you specify the VOI correctly?")
  
  cat("file \'" %+% filesUnrolled[VOI] %+% "\'\nduration 0.05\n", file="tempVoiFrameList.txt") 
  system(sprintf("ffmpeg -f concat -i tempVoiFrameList.txt -framerate 10 -vf eq=brightness=0.6:contrast=2 -c:v libx264 -preset slow -qp 18 -tune grain %s", videoName))
  
}


#' makeH264VideoFromSelectedFrames
#' 
#' @param inputBaseDirectory path containing "TSeries" subfolders.
#' @param VOI vector of volume indices that should be included in the video.
#' @param nPlanes total number of planes present in image series.
#' @param videoName string containing name of output video.
#' @param LAYOUT a list containing an scalar element "nx" (number of subpanels per row),
#'  element "params", a list containing parameters for each subpanel,
#'  optional element "rois" (an roi footprint matrix where rows are pixels and columns are rois),
#'  optional element "roiColors" containing colors to use for drawing ROIs. 
#'  
makeH264VideoFromSelectedFrames = function(inputBaseDirectory, VOI, nPlanes, videoName='video', LAYOUT){
  setwd(inputBaseDirectory)
  folders = list.dirs(inputBaseDirectory, recursive=FALSE, full.names=FALSE)
  folders = folders[grep('TSeries', folders)]
  
  files = unname(lapply(folders, function(f) paste(f, list.files(f, pattern = "tif+$", recursive=FALSE), sep='/')))
  files = unlist(files)
  channel = substr(files, start=regexpr("Ch", files), stop=regexpr("Ch", files)+2)
  frameIndex = as.numeric(substr(files, start=regexpr("Ch", files)+4, stop=regexpr("Ch", files)+9))
  plane = ((frameIndex-1) %% nPlanes) + 1
  volumeIndex = ceiling(frameIndex / nPlanes)
  
  cat("creating intermediate files...\n")
  dir.create('tempFrameFolder')
  
  require(parallel)
  cl=makeCluster(detectCores()-1)
  clusterEvalQ(cl, library(EBImage))
  clusterEvalQ(cl, library(dendRites))
  clusterEvalQ(cl, library(tiff))

  clusterExport(cl, c("plane","volumeIndex","channel","files", "LAYOUT", "VOI"), envir=environment())
  if(!is.null(LAYOUT$rois)){
    layoutMask = footprintMatrixToLabelMask(LAYOUT$rois, dim=c(768,768,1))  
    clusterExport(cl, "layoutMask", envir=environment())
  }
  
  parLapply(cl, 1:length(VOI), function(i) {
    imgList = lapply(LAYOUT$params, function(p){
      imgFile = files[volumeIndex==VOI[i] & channel==p$channel & plane==p$plane]
      img = pmin(readTIFF(imgFile, as.is=TRUE)/8191*p$scale,1)
      img = channel(img, paste('as',p$col,sep='')) # convert to color
      if(exists("layoutMask"))
        img = colorMask(mask=getComponentOutlineImage(layoutMask)[,,p$plane], bg=img, colors=LAYOUT$roiColors)
      img  
    })
    myImg = tile(combine(imgList), nx=LAYOUT$nx,lwd=0)
    writeTIFF(myImg, sprintf("tempFrameFolder/%06d.tif", i))
  })
  stopCluster(cl)

  # make subtitle file
  frameRate=10
  fmtTime = function(s) sprintf("%02d:%02d:%02d,%03d", floor(s/3600),floor(s/60)%%60, floor(s)%%60, round(1000*(s%%1)))
  for (i in 1:length(VOI)){
    startTime = (i-1)/frameRate
    stopTime = i/frameRate
    cat(sprintf("%d\n%s --> %s\n%0.1f seconds\n",i, fmtTime(startTime), fmtTime(stopTime), floor(VOI[i]/2)/10), file='subtitles.srt',append=(i!=1))
  }
  
  cat("Running FFMPEG...\n")
  system(sprintf("ffmpeg -y -framerate %d -i \"tempFrameFolder/%%06d.tif\" -c:v libx264 -preset medium -tune grain %s.mp4", frameRate, videoName))
  cat("Burning subtitle file into video.\n")
  system(sprintf("ffmpeg -y -i %s.mp4 -vf subtitles=subtitles.srt -c:v libx264 %s_subtitled.mp4", videoName, videoName))
}




makeH264VideoFromSelectedFrames(alignedBaseDirectory, VOI, 1, videoName='dendriteVideo',
                               LAYOUT=list(nx = 1, rois=NULL, roiColors='darkblue',
                                           params = list(list(channel='Ch1',plane=1, col='green',scale=2),
                                                         list(channel='Ch2',plane=1, col='red', scale=2))))




